import matplotlib.pyplot as plt
from wordcloud import WordCloud,STOPWORDS
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

from YouTubeSpeechRecognition import asr

# link = "https://www.youtube.com/watch?v=i3ku5nx4tMU"
link = "https://www.youtube.com/watch?v=sbCvQbBi2G8"
# text_file = open("/home/ming/Workspace/sentiment-analysis/speech.txt")
# data = text_file.read()
# text_file.close() 

data  = asr(link)

positive =[]
negative=[]
def sentimentWord(data):
    k=0
    sentence = data.split()
    while (k < len(sentence)):
        text = str(sentence[k])
        vs = SentimentIntensityAnalyzer().polarity_scores(text)
        print(str(k)+": "+text)
        k=k+1
        for i in vs:
            if (i == "pos"):
                if(vs[i] > 0):
                    print("==>Positive Word: "+text)
                    positive.append(text)
            if (i == "neg"):
                if(vs[i] > 0):
                    print("==>Negative Word: "+text)
                    negative.append(text)
                    
def plotWord(list,num):
    stopwords = set(STOPWORDS)
    stopwords.update(["br", "href"])
    wordcloud = WordCloud(stopwords=stopwords).generate(' '.join(list))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis("off")
    if(num==1):
        plt.savefig('positive.png')
    else:
        plt.savefig('negative.png')

sentimentWord(data)
newNegative = sorted(set(negative),key=lambda x:negative.index(x))
newPositive =  sorted(set(positive),key=lambda x:positive.index(x))
print("Positive word:")
print(newPositive)
print("Negative word:")
print(newNegative)
plotWord(newPositive,1)
plotWord(newNegative,0)

print("="*100)
print(data)
