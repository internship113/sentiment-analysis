import os
import torch
import youtube_dl
from tqdm import tqdm
from pyannote.audio import Model
from pyannote.audio.pipelines import VoiceActivityDetection
from transformers import pipeline
from collections import Counter
import cld3

PYANNOTEMODEL = Model.from_pretrained("pyannote_model.bin")
HYPER_PARAMETERS = {
  # onset/offset activation thresholds
  "onset": 0.5, "offset": 0.5,
  # remove speech regions shorter than that many seconds.
  "min_duration_on": 0.0,
  # fill non-speech regions shorter than that many seconds.
  "min_duration_off": 0.0
}

class YouTubeSpeechRecognition:
    def __init__(self, link) -> None:
        self.link = link
        self.lang_support = ['th', 'en']
        self.ydl_opts = {
            'outtmpl': 'file.%(ext)s',
            'format': 'bestaudio',
            'postprocessors': [{
                'key': 'FFmpegExtractAudio',
                'preferredcodec': 'wav',
            }]}
        self.info = youtube_dl.YoutubeDL(
            self.ydl_opts).extract_info(self.link, download=False)
        self.lang = self.YouTubeLanguageDetection(self.info)
        self.modelID = ['wannaphong/wav2vec2-large-xlsr-53-th-cv8-newmm' if self.lang ==
                        'th' else 'jonatasgrosman/wav2vec2-large-xlsr-53-english'][0]
        assert self.lang in self.lang_support, f'this tools support only {",".join(self.lang_support)} language'
        self.pipeline = pipeline(model=self.modelID,
                                 tokenizer=self.modelID,
                                 task='automatic-speech-recognition')
        self.pyannotepipeline = VoiceActivityDetection(segmentation=PYANNOTEMODEL)
        self.pyannotepipeline.instantiate(HYPER_PARAMETERS)
        self.device_num = [0 if torch.cuda.is_available() else -1][0]
        self.STT = ''

    def YouTubeLanguageDetection(self,infomation):
        def TagsDetect(infomation):
            try:
                tags = []
                for i in infomation['tags']:
                    tags.append(cld3.get_frequent_languages(i, num_langs=3))
                tags_lang = [
                    item.language for sublist in tags for item in sublist]
                tags_Prob = [
                    item.probability for sublist in tags for item in sublist]
                return dict(zip(tags_lang, tags_Prob))
            except:
                return dict()

        def TitleDetect(infomation):
            try:
                title = cld3.get_frequent_languages(
                    infomation['title'], num_langs=3)
                title_lang = [sublist.language for sublist in title]
                title_Prob = [sublist.probability for sublist in title]
                return dict(zip(title_lang, title_Prob))
            except:
                return dict()

        def DescriptionDetect(infomation):
            try:
                description = cld3.get_frequent_languages(
                    infomation['description'], num_langs=3)
                description_lang = [
                    sublist.language for sublist in description]
                description_Prob = [
                    sublist.probability for sublist in description]
                return dict(zip(description_lang, description_Prob))
            except:
                return dict()

        title_lang = Counter(TitleDetect(infomation))
        description_lang = Counter(DescriptionDetect(infomation))
        tag_lang = Counter(TagsDetect(infomation))
        languageYouTube = dict(title_lang+description_lang+tag_lang)
        result = {key: value / 3 for key, value in languageYouTube.items()}
        return max(result, key=result.get)

    def __download_audio__(self):
        try:
            ydl = youtube_dl.YoutubeDL(self.ydl_opts)
            ydl.download([self.link])
            info_with_audio_extension = dict(self.info)
            info_with_audio_extension['ext'] = 'wav'
            return ydl.prepare_filename(info_with_audio_extension)
        except Exception as e:
            os.remove('file.wav')
            print(e)

    def __tqdm_enumerate__(self, iter):
        i = 0
        for y in tqdm(iter):
            yield i, y
            i += 1

    def __ASR__(self):
        try:
            file = self.__download_audio__()
            output = self.pyannotepipeline(file)
            for i, speech in self.__tqdm_enumerate__(output.get_timeline().support()):
                text = str(speech).replace(']', '')
                text = text.replace('[', '')
                tm = text.strip().split(' -->  ')
                os.system(
                    f'ffmpeg  -i {file} -ss {tm[0]} -to {tm[1]}  file_{i}.wav -loglevel quiet')
                pipe = self.pipeline
                text = pipe(f'file_{i}.wav', chunk_length_s=10,
                            stride_length_s=(4, 2), device=self.device_num)
                self.STT += text['text']
                os.remove(f'file_{i}.wav')
            os.remove('file.wav')
        except Exception as e:
            os.remove('file.wav')
            print(e)


def asr(link:str) -> str:
    text = YouTubeSpeechRecognition(link)
    text.__ASR__()
    return text.STT
